=begin
 Crie uma função que dado um array de inteiros, faça um Cast para String e retorne um array com os valores em string.
Ex: Entrada: [25, 35, 45]
    Saída: ["25", "35", "45"]
=end

def string(array)
    # declara o array que vai ser retornado
    array = []

    # para cada valor no array inicial é criada uma string no array
    array.each do |num|
        array = array.push(num.to_s)
    end

    # retorna o array de strings
    return array
end

# chamando a função
print string([25, 35, 45])
