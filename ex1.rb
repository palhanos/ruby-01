=begin
 Crie uma função que dado um array de arrays, imprima na tela a soma e a multiplicação de todos os valores. 
 Ex: Entrada: [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
    Impressão na tela: Soma: 39
   Impressão na tela: Multiplicação: 100800
=end


def operate_array(arrays)
    # Variáveis que vão armazenar os valores
    sum = 0
    mult = 1

    # Percorre os elementos dos arrys
    arrays.each do |array|
        array.each do |num|
            sum = sum + num
            mult = mult * num
        end
    end
    
    # Imprime o resultado
    puts "Soma #{sum}"
    puts "Multiplicação #{mult}"
end

# Definindo o ararry que será usado e chamando a função
teste = [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
operate_array(teste)
