=begin
 Crie uma função que dado um array de inteiros, retorne um array apenas com os divisíveis por 3.
Ex: Entrada: [3, 6, 7, 8]
    Saída: [3, 6]
=end

def filter_array(array)
    # Declara o array que ira retornar no final
    new_array = []

    # cada número do array colocado como parametro, é checado se ele é
    # divisível por 3. Se ele for, é adicionado na lista final
    array.each do |num|
        if num % 3 == 0
            new_array = new_array.push(num)
        end
    end

    # Retorna o array do final
    return new_array
end

# Chamando a função
print filter_array([3, 6, 7, 8])
