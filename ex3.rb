=begin
Crie uma função, que dado um hash como parâmetro, e retorne um array com todos os valores que estão no hash elevados ao quadrado.
Ex: Entrada: {:chave1 => 5, :chave2 => 30, :chave3 => 20}
    Saída: [25, 900, 400]
=end

def ao_quadrado(hash)
    # criando o array que será retornado
    array = []

    # para cada valor no hash é adicionado o valor ao quadrado no array
    hash.keys.each do |key|
        array = array.push(hash[key] ** 2)
    end

    # retornando o array
    return array
end

# chamando a função
puts ao_quadrado( {:chave1 => 5, :chave2 => 30, :chave3 => 20})
