=begin
Crie uma função que dado dois valores, retorne a divisão deles (a divisão deve retornar um float).
Ex: Entrada: 5, 2
    Saída: 2.5
=end

# funcao que retorna a divisao de um numero por outro
def divide_numbers(num1, num2)
    return num1.to_f / num2.to_f
end

# Chama a função que divide os números
puts divide_numbers(6, 90)